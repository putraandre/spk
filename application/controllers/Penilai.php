<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penilai extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata['users_type'] !== 'penilai') {
			$this->session->sess_destroy();
			return redirect(base_url('index.php/login'));
		}
	}
	public function index()
	{
		$periode = $this->db->select('alternatif_periode')->from('alternatif')->group_by('alternatif_periode')->get()->result_array();
		$data['kriteria'] = $this->master_model->read('kriteria');
		$data['users'] = $this->db->get_where('users', ['users_type' => 'alternatif'])->result_array();
		$alternatif = $this->master_model->read('alternatif');
		foreach ($periode as $p){
			foreach ($data['users'] as $key => $value) {
				foreach ($data['kriteria'] as $v) {
					$data['periode'][$p['alternatif_periode']]['users'][$value['users_id']]['kriteria'][$v->kriteria_id] = '';
					foreach ($alternatif as $val) {
						if ($val->alternatif_users_id == $value['users_id'] && $val->alternatif_kriteria_id == $v->kriteria_id && $p['alternatif_periode'] == $val->alternatif_periode) {
							$data['periode'][$p['alternatif_periode']]['users'][$value['users_id']]['kriteria'][$v->kriteria_id] = [
								'alternatif_keterangan' => $val->alternatif_keterangan,
								'alternatif_id' => $val->alternatif_id,
								'alternatif_nilai' => $val->alternatif_nilai
							];
						}
					}
				}
			}
		}
		$this->template->display('pages/penilai/index',$data);
	}

	public function simpan(){
		$result=[];
		$data = $this->input->post();
		foreach($data['nilai'] as $key => $value){
			$this->db->where('alternatif_id', $key)
				->update('alternatif', [
					'alternatif_nilai'=>$value,
			]);
		}
		echo json_encode(true);
	}

	public function hasil(){
		$periode = $this->db->select('alternatif_periode')->from('alternatif')->group_by('alternatif_periode')->get()->result_array();
		$data['kriteria'] = $this->master_model->read('kriteria');
		$data['users'] = $this->db->get_where('users', ['users_type' => 'alternatif'])->result_array();
		$alternatif = $this->master_model->read('alternatif');
		foreach ($periode as $p){
			foreach ($data['users'] as $key => $value) {
				foreach ($data['kriteria'] as $v) {
					$data['periode'][$p['alternatif_periode']]['users'][$value['users_id']]['kriteria'][$v->kriteria_id] = '';
					foreach ($alternatif as $val) {
						if ($val->alternatif_users_id == $value['users_id'] && $val->alternatif_kriteria_id == $v->kriteria_id && $p['alternatif_periode'] == $val->alternatif_periode) {
							$data['periode'][$p['alternatif_periode']]['users'][$value['users_id']]['kriteria'][$v->kriteria_id] = [
								'alternatif_keterangan' => $val->alternatif_keterangan,
								'alternatif_id' => $val->alternatif_id,
								'alternatif_nilai' => $val->alternatif_nilai
							];
						}
					}
				}
			}
		}

		$this->template->display('pages/penilai/hasil',$data);
	}
}
