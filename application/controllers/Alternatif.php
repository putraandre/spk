<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Alternatif extends CI_Controller {
	public $users_id;
	public $periode;
	public function __construct()
	{
		parent::__construct();

		if ($this->session->userdata['users_type'] !== 'alternatif') {
			$this->session->sess_destroy();
			return redirect(base_url('index.php/login'));
		}
		$this->users_id = $this->session->userdata('users_id');
		$this->periode = date('Y');
	}
	public function index()
	{
		$data['form'] = $this->master_model->read('kriteria');
		$data['data'] = $this->db->get_where('alternatif',['alternatif_users_id'=>$this->users_id,'alternatif_periode'=>$this->periode])->result();
		$this->template->display('pages/ukm/index',$data);
	}
	public function simpan()
	{
		foreach ($_POST['kriteria'] as $key => $value){

			$check = $this->db->get_where('alternatif',
				[
					'alternatif_users_id'=>$this->users_id,
					'alternatif_periode'=>$this->periode,
					'alternatif_kriteria_id'=>$key
				]
			)->row_array();
			$data = [
				'alternatif_users_id'=>$this->users_id,
				'alternatif_kriteria_id'=>$key,
				'alternatif_periode'=>$this->periode,
				'alternatif_keterangan'=>$value,
			];
			if (is_null($check)) {
				$this->db->insert('alternatif', $data);
			} else {

				$this->db->update('alternatif', $data,[
					'alternatif_id'=> $check['alternatif_id']
				]);
			}
		}
		redirect('alternatif');
	}
}
