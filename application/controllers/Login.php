<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function index()
	{
		if (!is_null(@$this->session->userdata['users_type'])) {
			return redirect(base_url('index.php/').''.$this->session->userdata['users_type']);
		}
		return $this->load->view('pages/login');
	}

	public function post(){
		$data = $this->input->post();
		$data = $this->db->get_where('users', ["users_name" => $data["users_name"],"users_password" => md5($data["users_password"]) ])->row_array();
		if (!is_null($data)) {
			$this->session->set_userdata($data);
			return redirect(base_url('index.php/').''.$data['users_type']);
		}
		$this->session->set_flashdata('error','<div class="alert alert-danger" role="alert">Username atau Password Salah!</div>');
		return redirect(base_url('index.php/login'));
	}

	public function logout()
	{
		$this->session->sess_destroy();
		return redirect(base_url('index.php/login'));
	}
}
