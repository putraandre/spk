<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master extends CI_Controller {
	public function index()
	{
		$this->load->view('template');
    }
    public function users(){
		$data = [
			[
				'users_name' => 'admin',
				'users_password' => md5('password'),
				'users_nama' => 'admin',
				'users_type' => 'admin'
			],
			[
				'users_name' => 'penilai1',
				'users_password' => md5('password'),
				'users_nama' => 'penilai1',
				'users_type' => 'penilai'
			],
			[
				'users_name' => 'penilai2',
				'users_password' => md5('password'),
				'users_nama' => 'penilai2',
				'users_type' => 'penilai'
			],
			[
				'users_name' => 'mapala',
				'users_password' => md5('password'),
				'users_nama' => 'UKM Mapala',
				'users_type' => 'alternatif'
			],
			[
				'users_name' => 'kesenian',
				'users_password' => md5('password'),
				'users_nama' => 'UKM Kesenian',
				'users_type' => 'alternatif'
			],
			[
				'users_name' => 'informatikakomputer',
				'users_password' => md5('password'),
				'users_nama' => 'UKM Informatika & Komputer',
				'users_type' => 'alternatif'
			],
			[
				'users_name' => 'futsal',
				'users_password' => md5('password'),
				'users_nama' => 'UKM Futsal',
				'users_type' => 'alternatif'
			],
			[
				'users_name' => 'basket',
				'users_password' => md5('password'),
				'users_nama' => 'UKM Basket',
				'users_type' => 'alternatif'
			],
			[
				'users_name' => 'taekWondo',
				'users_password' => md5('password'),
				'users_nama' => 'UKM TaekWondo',
				'users_type' => 'alternatif'
			],
		];
		foreach ($data as $key => $value){
			$this->master_model->create('users',$value);
		}
		$data = [
			[
				'kriteria_nama' => 'Program Kerja',
				'kriteria_type' => 'benefit',
				'kriteria_bobot' => 50,
			],
			[
				'kriteria_nama' => 'PKM',
				'kriteria_type' => 'benefit',
				'kriteria_bobot' => 25,
			],
			[
				'kriteria_nama' => 'Lomba-lomba',
				'kriteria_type' => 'benefit',
				'kriteria_bobot' => 15,
			],
			[
				'kriteria_nama' => 'Anggota Aktif',
				'kriteria_type' => 'benefit',
				'kriteria_bobot' => 10,
			],
		];
		foreach ($data as $key => $value){
			$this->master_model->create('kriteria',$value);
		}

		print_r($this->master_model->read('users'));
		print_r($this->master_model->read('kriteria'));
		print_r($this->master_model->read('alternatif'));
	}

	public function alternatif(){
		$users = $this->db->get_where('users',['users_type'=>'alternatif'])->result();
		$kriteria = $this->db->get('kriteria')->result();

		foreach ($users as $i) {
			print_r($i->users_id);
			foreach ($kriteria as $j){
				print_r($j->kriteria_id);
				$this->db->insert('alternatif', [
				'alternatif_users_id' => $i->users_id,
				'alternatif_kriteria_id' => $j->kriteria_id,
				'alternatif_periode' => 2020,
				'alternatif_keterangan' => $j->kriteria_id != 4 ? $j->kriteria_nama.' '.$i->users_nama : rand(30,100),
				]);





			}
		}
		print_r($this->db->get('alternatif')->result());
	}
	public function sample(){
		$data = $this->db->select('alternatif_periode')->from('alternatif')->group_by('alternatif_periode')->get()->result();
		foreach ($data as $item){
			echo $item->alternatif_periode;
		}die();
	}
}
