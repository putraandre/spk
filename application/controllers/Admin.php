<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata['users_type'] !== 'admin') {
			$this->session->sess_destroy();
			return redirect(base_url('index.php/login'));
		}
	}

	public function index()
	{
		$this->template->display('pages/admin/index');
	}

	public function pengguna($id=false)
	{
		if ($id){
			$input = $this->master_model->read('users',$id);
			if ($input) {
				$data['input']=$input;
			}
		}
		$data['data'] = $this->master_model->read('users');
		$this->template->display('pages/admin/pengguna',$data);
	}

	public function penggunasimpan()
	{
		$input = array_filter($this->input->post());
		if (!is_null($input['users_password'])){
			$input['users_password'] = md5($input['users_password']);
		}
		if (!is_null($this->input->post('users_id'))){
			$data = $this->master_model->update('users',$input);
		}else{
			$data = $this->master_model->create('users',$input);
		}
		if ($data){
			$this->session->set_flashdata('form','<div class="alert alert-primary" role="alert">Data Berhasil Tersimpan</div>');
		} else {
			$this->session->set_flashdata('form','<div class="alert alert-primary" role="alert">Data Gagal Tersimpan</div>');
		}
		return redirect(base_url('index.php/admin/pengguna'));
	}

	public function penggunahapus($id)
	{
		$data = $this->master_model->delete('users',$id);
		if ($data){
			$this->session->set_flashdata('form','<div class="alert alert-primary" role="alert">Data Telah Terhapus</div>');
		} else {
			$this->session->set_flashdata('form','<div class="alert alert-primary" role="alert">Gagal Menghapus Data</div>');
		}
		return redirect(base_url('index.php/admin/pengguna'));
	}

	public function kriteria($id=false)
	{
		if ($id){
			$input = $this->master_model->read('kriteria',$id);
			if ($input) {
				$data['input']=$input;
			}
		}
		$data['data'] = $this->master_model->read('kriteria');
		$this->template->display('pages/admin/kriteria',$data);
	}

	public function kriteriasimpan()
	{
		$input = array_filter($this->input->post());
		if (!is_null($this->input->post('kriteria_id'))){
			$data = $this->master_model->update('kriteria',$input);
		}else{
			$data = $this->master_model->create('kriteria',$input);
		}
		if ($data){
			$this->session->set_flashdata('form','<div class="alert alert-primary" role="alert">Data Berhasil Tersimpan</div>');
		} else {
			$this->session->set_flashdata('form','<div class="alert alert-primary" role="alert">Data Gagal Tersimpan</div>');
		}
		return redirect(base_url('index.php/admin/kriteria'));
	}

	public function kriteriahapus($id)
	{
		$data = $this->master_model->delete('kriteria',$id);
		if ($data){
			$this->session->set_flashdata('form','<div class="alert alert-primary" role="alert">Data Telah Terhapus</div>');
		} else {
			$this->session->set_flashdata('form','<div class="alert alert-primary" role="alert">Gagal Menghapus Data</div>');
		}
		return redirect(base_url('index.php/admin/kriteria'));
	}

	public function alternatif()
	{
		$periode = $this->db->select('alternatif_periode')->from('alternatif')->group_by('alternatif_periode')->get()->result_array();
		$data['kriteria'] = $this->master_model->read('kriteria');
		$data['users'] = $this->db->get_where('users', ['users_type' => 'alternatif'])->result_array();
		$alternatif = $this->master_model->read('alternatif');
		foreach ($periode as $p){
			foreach ($data['users'] as $key => $value) {
				$index = 0;
				foreach ($data['kriteria'] as $v) {
					$data['periode'][$p['alternatif_periode']]['users'][$key]['kriteria'][$index] = '';
					foreach ($alternatif as $val) {
						if ($val->alternatif_users_id == $value['users_id'] && $val->alternatif_kriteria_id == $v->kriteria_id && $p['alternatif_periode'] == $val->alternatif_periode) {
							$data['periode'][$p['alternatif_periode']]['users'][$key]['kriteria'][$index] = $val->alternatif_keterangan;
						}
					}
					$index++;
				}
			}
		}
		$this->template->display('pages/admin/alternatif',$data);
	}
	public function simpanpenilaian(){
		$result=[];
		$data = $this->input->post();
		foreach($data['nilai'] as $key => $value){
			$this->db->where('alternatif_id', $key)
				->update('alternatif', [
					'alternatif_nilai'=>$value,
			]);
		}
		echo json_encode(true);
	}
	public function penilaian()
	{
		$periode = $this->db->select('alternatif_periode')->from('alternatif')->group_by('alternatif_periode')->get()->result_array();
		$data['kriteria'] = $this->master_model->read('kriteria');
		$data['users'] = $this->db->get_where('users', ['users_type' => 'alternatif'])->result_array();
		$alternatif = $this->master_model->read('alternatif');
		foreach ($periode as $p){
			foreach ($data['users'] as $key => $value) {
				foreach ($data['kriteria'] as $v) {
					$data['periode'][$p['alternatif_periode']]['users'][$value['users_id']]['kriteria'][$v->kriteria_id] = '';
					foreach ($alternatif as $val) {
						if ($val->alternatif_users_id == $value['users_id'] && $val->alternatif_kriteria_id == $v->kriteria_id && $p['alternatif_periode'] == $val->alternatif_periode) {
							$data['periode'][$p['alternatif_periode']]['users'][$value['users_id']]['kriteria'][$v->kriteria_id] = [
								'alternatif_keterangan' => $val->alternatif_keterangan,
								'alternatif_id' => $val->alternatif_id,
								'alternatif_nilai' => $val->alternatif_nilai
							];
						}
					}
				}
			}
		}
		$this->template->display('pages/admin/penilaian',$data);
	}

	public function hasil(){
		$periode = $this->db->select('alternatif_periode')->from('alternatif')->group_by('alternatif_periode')->get()->result_array();
		$data['kriteria'] = $this->master_model->read('kriteria');
		$data['users'] = $this->db->get_where('users', ['users_type' => 'alternatif'])->result_array();
		$alternatif = $this->master_model->read('alternatif');
		foreach ($periode as $p){
			foreach ($data['users'] as $key => $value) {
				foreach ($data['kriteria'] as $v) {
					$data['periode'][$p['alternatif_periode']]['users'][$value['users_id']]['kriteria'][$v->kriteria_id] = '';
					foreach ($alternatif as $val) {
						if ($val->alternatif_users_id == $value['users_id'] && $val->alternatif_kriteria_id == $v->kriteria_id && $p['alternatif_periode'] == $val->alternatif_periode) {
							$data['periode'][$p['alternatif_periode']]['users'][$value['users_id']]['kriteria'][$v->kriteria_id] = [
								'alternatif_keterangan' => $val->alternatif_keterangan,
								'alternatif_id' => $val->alternatif_id,
								'alternatif_nilai' => $val->alternatif_nilai
							];
						}
					}
				}
			}
		}

		$this->template->display('pages/admin/hasil',$data);
	}
}
