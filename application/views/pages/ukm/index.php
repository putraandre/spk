<div class="col-md-12">
	<div class="bgc-white p-20 bd mb-4">
		<?= @$this->session->flashdata('form'); ?>
		<h6 class="c-grey-900">Form Data UKM/HMJ Periode <?= date('Y') ?></h6>
		<div class="mT-30">
			<form action="<?= base_url('index.php/alternatif/simpan')?>" method="post">
				<?php
				if (@$form){
					$i=0;
					foreach (@$form as $key => $value) {
						echo '<div class="form-group row">
					<label for="users_name" class="col-sm-2 col-form-label">'.$value->kriteria_nama.'</label>
					<div class="col-sm-10">
						<textarea class="form-control" id="kriteria-'.$value->kriteria_id.'" name="kriteria['.$value->kriteria_id.']" rows="3">'.@$data[$i]->alternatif_keterangan.'</textarea>
					</div>
				</div>';
						$i++;
					}
				}else{
					echo 'Harap Mengisi data kriteria';
				}

				?>

				<div class="form-group row">
					<div class="col-sm-10">
						<button type="submit" class="btn btn-primary">Simpan</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
