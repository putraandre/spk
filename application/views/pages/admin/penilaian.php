<div class="col-md-12">
	<div class="bgc-white bd bdrs-3 p-20 mB-20">
		<ul class="nav nav-tabs" role="tablist">
			<?php
			$i = 1;
			foreach ($periode as $item => $v) {
				$active = $i === 1 ? ' active' : '';
				echo '<li class="nav-item">
<a class="nav-link' . $active . '" data-toggle="tab" href="#p-' . $item . '">' . $item . '</a></li>';
				$i++;
			}
			?>
		</ul>
		<div class="tab-content">
			<?php
			$i = 1;
			foreach ($periode as $item => $v) {
				$active = $i === 1 ? ' show active' : '';
				echo '<div class="tab-pane fade' . $active . '" id="p-' . $item . '" role="tabpanel" aria-labelledby="' . $item . '-tab">
<h4 class="c-grey-900 mB-20">Data Alternatif Periode ' . $item . '</h4>
			<table class="table">
				<thead>
				<tr>
					<th>#</th>
					<th>Alternatif(UKM/HMJ)</th>';
				$nok = 1;
				if (!is_null(@$kriteria)) {
					foreach (@$kriteria as $value) {
						echo '<th>' . $value->kriteria_nama . '</th>';
						$nok++;
					}
				}
				echo '
				</tr>
				</thead>
				<tbody>';
				$no = 1;
				if (!is_null(@$users)) {
					foreach (@$users as $kk => $value) {
						echo '<tr>
							<td>' . $no . '</td>
							<td>' . $value['users_nama'] . '</td>';
						foreach ($v['users'][$value['users_id']]['kriteria'] as $key => $val) {
							echo '<td>' . @$val['alternatif_keterangan'] . '</td>';
						}
						echo '<tr>';
						$no++;
					}
				} else {
					echo 'Data Kosong';
				}
				echo '	</tbody>
            </table>
            <hr>
            <br><br>
            <h4 class="c-grey-900 mB-20">Data Penilaian Periode ' . $item . '</h4>
            <table class="table">
				<thead>
				<tr>
					<th>#</th>
					<th>Alternatif(UKM/HMJ)</th>';
				$nok = 1;
				if (!is_null(@$kriteria)) {
					foreach (@$kriteria as $value) {
						echo '<th>' . $value->kriteria_nama . '</th>';
						$nok++;
					}
				}
				echo '
				</tr>
				</thead>
				<tbody>';
				$no = 1;
				if (!is_null(@$users)) {
					foreach (@$users as $kk => $value) {
						echo '<tr>
							<td>' . $no . '</td>
							<td>' . $value['users_nama'] . '</td>';
						foreach ($v['users'][$value['users_id']]['kriteria'] as $key => $val) {
                            $input = @$val['alternatif_nilai'] ?:  0;
							echo '<td><input type="number" min="1" max="100" class="form-control" placeholder="' . @$val['alternatif_keterangan'] . '" name="nilai['.@$val['alternatif_id'].']" value="'.$input.'" /></td>';
						}
						echo '<tr>';
						$no++;
                    }
				} else {
					echo 'Data Kosong';
				}
				echo '	</tbody>
            </table>
            <hr>
            <button class="btn btn-primary" onclick="simpan()">Simpan</button>
			</div>';
				$i++;
			}
			?>

		</div>

	</div>
</div>
<script src="//code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
    $(function(){
        console.log('<?= base_url()?>');
    })
    function simpan() {
        let result = {};
        let input =  document.querySelectorAll("input");
        Array.from(input).forEach(function(el) {
           el.value != '' ? result[el.name] = el.value : null;
        });
        $.ajax({
            method: "POST",
            url: `<?= base_url()?>index.php/admin/simpanpenilaian`,
            data: result
        })
        .done(function(data) {
            data ? location.reload() : alert('Gagal Menambah Data')
        });
    };
</script>
