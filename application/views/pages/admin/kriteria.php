<div class="col-md-12">
	<div class="bgc-white p-20 bd mb-4">
		<?= @$this->session->flashdata('form'); ?>
		<h6 class="c-grey-900">Form Kriteria</h6>
		<div class="mT-30">
			<form action="<?= base_url('index.php/admin/kriteriasimpan')?>" method="post">
				<?= @$input->kriteria_id ? '<input type="hidden" required class="form-control" id="kriteria_id" name="kriteria_id" value="'.@$input->kriteria_id.'">' : ''?>
				<div class="form-group row">
					<label for="kriteria_nama" class="col-sm-2 col-form-label">Nama</label>
					<div class="col-sm-10">
						<input type="text" required class="form-control" id="kriteria_nama" name="kriteria_nama" placeholder="Nama" value="<?= @$input->kriteria_nama?>">
					</div>
				</div>

				<div class="form-group row">
					<label for="kriteria_type" class="col-sm-2 col-form-label">Tipe Kriteria</label>
					<div class="col-sm-10">
						<select name="kriteria_type" id="kriteria_type" class="form-control" required>
							<option value="">Pilih Tipe</option>
							<option value="benefit" <?= @$input->kriteria_type === 'benefit' ? 'selected' : '' ?>>Benefit</option>
							<option value="cost" <?= @$input->kriteria_type === 'cost' ? 'selected' : '' ?>>Cost</option>
						</select>
					</div>
				</div>
				<div class="form-group row">
					<label for="kriteria_bobot" class="col-sm-2 col-form-label">Bobot</label>
					<div class="col-sm-10">
						<div class="input-group mb-3">
							<input type="number" class="form-control" id="kriteria_bobot" name="kriteria_bobot" placeholder="Bobot Kriteria" value="<?= @$input->kriteria_bobot?>">
							<div class="input-group-append">
								<span class="input-group-text">%</span>
							</div>
						</div>

					</div>
				</div>
				<div class="form-group row">
					<div class="col-sm-10">
						<button type="submit" class="btn btn-primary">Simpan</button>
						<a href="<?= base_url('index.php/admin/kriteria')?>" type="button" class="btn btn-info">Clear</a>
					</div>
				</div>
			</form>
		</div>
	</div>

	<div class="bgc-white bd bdrs-3 p-20 mB-20">
		<h4 class="c-grey-900 mB-20">Data Kriteria</h4>
		<table class="table">
			<thead>
			<tr>
				<th>#</th>
				<th>Nama Kriteria</th>
				<th>Tipe</th>
				<th>Bobot</th>
				<th>Aksi</th>
			</tr>
			</thead>
			<tbody>
			<?php
			$no = 1;
			if (!is_null(@$data)){
				foreach (@$data as $value) {
					echo '<tr>
							<td>'.$no.'</td>
							<td>'.$value->kriteria_nama.'</td>
							<td>'.$value->kriteria_type.'</td>
							<td>'.$value->kriteria_bobot.'</td>
							<td>
							<a href="'.base_url('index.php/admin/kriteria').'/'.$value->kriteria_id.'" class="btn btn-info"><i class="fa fa-edit"></i></a>
							<a href="'.base_url('index.php/admin/kriteriahapus').'/'.$value->kriteria_id.'" class="btn btn-danger"><i class="fa fa-trash-o"></i></a>
							</td>
						<tr>';
					$no++;
				}
			} else {
				echo 'Data Kosong';
			}

			?>
			</tbody>
		</table>
	</div>
</div>
