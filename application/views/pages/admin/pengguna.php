	<div class="col-md-12">
		<div class="bgc-white p-20 bd mb-4">
			<?= @$this->session->flashdata('form'); ?>
			<h6 class="c-grey-900">Form Pengguna</h6>
			<div class="mT-30">
				<form action="<?= base_url('index.php/admin/penggunasimpan')?>" method="post">
					<?= @$input->users_id ? '<input type="hidden" required class="form-control" id="users_id" name="users_id" value="'.@$input->users_id.'">' : ''?>

					<div class="form-group row">
						<label for="users_name" class="col-sm-2 col-form-label">Username</label>
						<div class="col-sm-10">
							<input type="text" required class="form-control" id="users_name" name="users_name" placeholder="Username" value="<?= @$input->users_name?>">
						</div>
					</div>
					<div class="form-group row">
						<label for="users_nama" class="col-sm-2 col-form-label">Nama</label>
						<div class="col-sm-10">
							<input type="text" required class="form-control" id="users_nama" name="users_nama" placeholder="Nama" value="<?= @$input->users_nama?>">
						</div>
					</div>
					<div class="form-group row">
						<label for="users_password" class="col-sm-2 col-form-label">Password</label>
						<div class="col-sm-10">
							<input type="password" class="form-control" id="users_password" name="users_password" placeholder="Password">
						</div>
					</div>
					<div class="form-group row">
						<label for="users_type" class="col-sm-2 col-form-label">Level</label>
						<div class="col-sm-10">
							<select name="users_type" id="users_type" class="form-control" required>
								<option value="">Pilih Hak Akses</option>
								<option value="admin" <?= @$input->users_type === 'admin' ? 'selected' : '' ?>>Admin</option>
								<option value="penilai" <?= @$input->users_type === 'penilai' ? 'selected' : '' ?>>Penilai</option>
								<option value="alternatif" <?= @$input->users_type === 'alternatif' ? 'selected' : '' ?>>Alternatif</option>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-sm-10">
							<button type="submit" class="btn btn-primary">Simpan</button>
							<a href="<?= base_url('index.php/admin/pengguna')?>" type="button" class="btn btn-info">Clear</a>
						</div>
					</div>
				</form>
			</div>
		</div>

		<div class="bgc-white bd bdrs-3 p-20 mB-20">
			<h4 class="c-grey-900 mB-20">Data Pengguna</h4>
			<table class="table">
				<thead>
				<tr>
					<th>#</th>
					<th>Username</th>
					<th>Nama</th>
					<th>Level</th>
					<th>Aksi</th>
				</tr>
				</thead>
				<tbody>
				<?php
				$no = 1;
				if (!is_null(@$data)){
					foreach (@$data as $value) {
						echo '<tr>
							<td>'.$no.'</td>
							<td>'.$value->users_name.'</td>
							<td>'.$value->users_nama.'</td>
							<td>'.$value->users_type.'</td>
							<td>
							<a href="'.base_url('index.php/admin/pengguna').'/'.$value->users_id.'" class="btn btn-info"><i class="fa fa-edit"></i></a>
							<a href="'.base_url('index.php/admin/penggunahapus').'/'.$value->users_id.'" class="btn btn-danger"><i class="fa fa-trash-o"></i></a>
							</td>
						<tr>';
					$no++;
					}
				} else {
					echo 'Data Kosong';
				}

				?>
				</tbody>
			</table>
		</div>
	</div>
