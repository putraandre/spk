<div class="col-md-12">
	<div class="bgc-white bd bdrs-3 p-20 mB-20">
		<ul class="nav nav-tabs" role="tablist">
			<?php
			$i = 1;
			foreach ($periode as $item => $v) {
				$active = $i === 1 ? ' active' : '';
				echo '<li class="nav-item">
<a class="nav-link' . $active . '" data-toggle="tab" href="#p-' . $item . '">' . $item . '</a></li>';
				$i++;
			}
			?>
		</ul>
		<div class="tab-content">
			<?php
			$i = 1;
			foreach ($periode as $item => $v) {
				$active = $i === 1 ? ' show active' : '';
				echo '<div class="tab-pane fade' . $active . '" id="p-' . $item . '" role="tabpanel" aria-labelledby="' . $item . '-tab">
            <h4 class="c-grey-900 mB-20">Data Penilaian Periode ' . $item . '</h4>
            <table class="table">
				<thead>
				<tr>
					<th>#</th>
					<th>Alternatif(UKM/HMJ)</th>';
				$nok = 1;
				if (!is_null(@$kriteria)) {
					foreach (@$kriteria as $value) {
						echo '<th>' . $value->kriteria_nama . '</th>';
						$nok++;
					}
				}
				echo '
				</tr>
				</thead>
				<tbody>';
				$no = 1;
				if (!is_null(@$users)) {
					$max = [];
					foreach (@$users as $kk => $value) {
						echo '<tr>
							<td>' . $no . '</td>
							<td>' . $value['users_nama'] . '</td>';
						foreach ($v['users'][$value['users_id']]['kriteria'] as $key => $val) {
							$input = @$val['alternatif_nilai'] == 0 ? 0 : $val['alternatif_nilai'];
							$max[$key][] = $input;
							echo '<td style="text-align: center">'.$input.'</td>';
						}
						echo '<tr>';
						$no++;
					}
					echo '<tr style="border-top: 2px solid black"><td></td><th>Nilai Maksimal</th>';
					foreach ($max as $maxk => $maxv){
						echo '<th style="text-align: center">'.max($max[$maxk]).'</th>';
					}

				} else {
					echo 'Data Kosong';
				}
				echo '</tbody></table>
            <hr>
            <br><br>
            <h4 class="c-grey-900 mB-20">Hasil Normalisasi</h4>
				<table class="table">
				<thead>
				<tr>
					<th>#</th>
					<th>Alternatif(UKM/HMJ)</th>';
				$nok = 1;
				if (!is_null(@$kriteria)) {
					foreach (@$kriteria as $value) {
						echo '<th>' . $value->kriteria_nama . ' </th>';
						$nok++;
					}
				}
				echo '
				</tr>
				</thead>
				<tbody>';
				$no = 1;
				if (!is_null(@$users)) {
					foreach (@$users as $kk => $value) {
						echo '<tr>
							<td>' . $no . '</td>
							<td>' . $value['users_nama'] . '</td>';
						foreach ($v['users'][$value['users_id']]['kriteria'] as $key => $val) {
							$input = @$val['alternatif_nilai'] == 0 ? 0 : $val['alternatif_nilai'];
							$input = $input === 0 ? 0 : $input / max($max[$key]);
							echo '<td style="text-align: center">'.number_format($input,2).'</td>';
						}
						echo '<tr>';
						$no++;
					}


				} else {
					echo 'Data Kosong';
				}
				echo '</tbody></table>
				<hr>
            <br><br>
            <h4 class="c-grey-900 mB-20">Hasil Normalisasi</h4>
				<table class="table hasil" >
				<thead>
				<tr>
					<th>#</th>
					<th>Alternatif(UKM/HMJ)</th>';
				$nok = 1;
				if (!is_null(@$kriteria)) {
					foreach (@$kriteria as $value) {
						echo '<th>' . $value->kriteria_nama . ' </th>';
						$nok++;
					}
					echo '<th>Hasil</th>';
				}
				echo '
				</tr>
				</thead>
				<tbody>';
				$no = 1;

				if (!is_null(@$users)) {
					foreach (@$users as $kk => $value) {
						$kriteriaIndex = 0;
						$hasil = 0;
						echo '<tr>
							<td>' . $no . '</td>
							<td>' . $value['users_nama'] . '</td>';
						foreach ($v['users'][$value['users_id']]['kriteria'] as $key => $val) {
							$input = @$val['alternatif_nilai'] == 0 ? 0 : $val['alternatif_nilai'];
							$input = $input === 0 ? 0 :  ($input / max($max[$key]) * ($kriteria[$kriteriaIndex]->kriteria_bobot/100));
							echo '<td style="text-align: center">'.number_format($input,2).'</td>';
							$hasil = $hasil + $input;
							$kriteriaIndex++;
						}
						echo '<td style="text-align: center">'.number_format($hasil,2).'</td></tr>';
						$no++;
					}
				} else {
					echo 'Data Kosong';
				}
				echo '</tbody></table>';
				echo '</div>';
				$i++;
			}
			?>

		</div>

	</div>
</div>
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.print.min.js"></script>
<script>
	$(document).ready(function() {
		$('.hasil').DataTable({
			dom: 'Bfrtip',
			buttons: [
				'pdf', 'print'
			]
		}).columns(-1).order('desc').draw();
	} );
</script>
