<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Master_model extends CI_Model
{
    public function create($table,$data){
        return $this->db->insert($table, $data);
    }
    public function read($table,$id=false){
        if (!$id) {
            return $this->db->get($table)->result();
        }
        return $this->db->get_where($table, [$table."_id" => $id])->row();
    }
    public function update($table,$data){
        return $this->db->update($table, $data, [$table.'_id'=> $data[$table.'_id']]);
    }
    public function delete($table,$data){
        return $this->db->delete($table, [$table.'_id'=> $data]);
    }

}
