-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 12, 2020 at 03:18 PM
-- Server version: 10.4.8-MariaDB-log
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `spk`
--

-- --------------------------------------------------------

--
-- Table structure for table `alternatif`
--

CREATE TABLE `alternatif` (
  `alternatif_id` int(11) NOT NULL,
  `alternatif_users_id` int(11) NOT NULL,
  `alternatif_kriteria_id` int(11) NOT NULL,
  `alternatif_periode` year(4) NOT NULL,
  `alternatif_keterangan` text NOT NULL,
  `alternatif_nilai` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `alternatif`
--

INSERT INTO `alternatif` (`alternatif_id`, `alternatif_users_id`, `alternatif_kriteria_id`, `alternatif_periode`, `alternatif_keterangan`, `alternatif_nilai`) VALUES
(1, 4, 1, 2019, 'Program Kerja 1', 2),
(2, 4, 2, 2019, 'PKM 2', 0),
(3, 4, 3, 2019, 'Lomba-lomba 3', 0),
(4, 4, 4, 2019, '33', 0),
(5, 5, 1, 2019, 'Program Kerja 1', 3),
(6, 5, 2, 2019, 'PKM 2', 0),
(7, 5, 3, 2019, 'Lomba-lomba 3', 0),
(8, 5, 4, 2019, '79', 0),
(9, 6, 1, 2019, 'Program Kerja 1', 4),
(10, 6, 2, 2019, 'PKM 2', 4),
(11, 6, 3, 2019, 'Lomba-lomba 3', 0),
(12, 6, 4, 2019, '87', 0),
(13, 7, 1, 2019, 'Program Kerja 1', 0),
(14, 7, 2, 2019, 'PKM 2', 0),
(15, 7, 3, 2019, 'Lomba-lomba 3', 0),
(16, 7, 4, 2019, '54', 0),
(17, 8, 1, 2019, 'Program Kerja 1', 0),
(18, 8, 2, 2019, 'PKM 2', 0),
(19, 8, 3, 2019, 'Lomba-lomba 3', 0),
(20, 8, 4, 2019, '70', 0),
(21, 9, 1, 2019, 'Program Kerja 1', 0),
(22, 9, 2, 2019, 'PKM 2', 0),
(23, 9, 3, 2019, 'Lomba-lomba 3', 0),
(24, 9, 4, 2019, '65', 0),
(25, 4, 1, 2020, 'Program Kerja UKM Mapala', 0),
(26, 4, 2, 2020, 'PKM UKM Mapala', 0),
(27, 4, 3, 2020, 'Lomba-lomba UKM Mapala', 0),
(28, 4, 4, 2020, '36', 0),
(29, 5, 1, 2020, 'Program Kerja UKM Kesenian', 0),
(30, 5, 2, 2020, 'PKM UKM Kesenian', 0),
(31, 5, 3, 2020, 'Lomba-lomba UKM Kesenian', 0),
(32, 5, 4, 2020, '33', 0),
(33, 6, 1, 2020, 'Program Kerja UKM Informatika & Komputer', 0),
(34, 6, 2, 2020, 'PKM UKM Informatika & Komputer', 0),
(35, 6, 3, 2020, 'Lomba-lomba UKM Informatika & Komputer', 0),
(36, 6, 4, 2020, '56', 0),
(37, 7, 1, 2020, 'Program Kerja UKM Futsalaa', 0),
(38, 7, 2, 2020, 'PKM UKM Futsalaa', 0),
(39, 7, 3, 2020, 'Lomba-lomba UKM Futsalaa', 0),
(41, 8, 1, 2020, 'Program Kerja UKM Basket', 0),
(42, 8, 2, 2020, 'PKM UKM Basket', 0),
(43, 8, 3, 2020, 'Lomba-lomba UKM Basket', 0),
(44, 8, 4, 2020, '93', 0),
(45, 9, 1, 2020, 'Program Kerja UKM TaekWondo', 0),
(46, 9, 2, 2020, 'PKM UKM TaekWondo', 0),
(47, 9, 3, 2020, 'Lomba-lomba UKM TaekWondo', 0),
(48, 9, 4, 2020, '85', 0),
(49, 7, 4, 2020, '122', 0);

-- --------------------------------------------------------

--
-- Table structure for table `kriteria`
--

CREATE TABLE `kriteria` (
  `kriteria_id` int(11) NOT NULL,
  `kriteria_nama` varchar(50) NOT NULL,
  `kriteria_type` enum('benefit','cost') NOT NULL,
  `kriteria_bobot` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kriteria`
--

INSERT INTO `kriteria` (`kriteria_id`, `kriteria_nama`, `kriteria_type`, `kriteria_bobot`) VALUES
(1, 'Program Kerja', 'benefit', 50),
(2, 'PKM', 'benefit', 25),
(3, 'Lomba-lomba', 'benefit', 15),
(4, 'Anggota Aktif', 'benefit', 10);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `users_id` int(11) NOT NULL,
  `users_name` varchar(100) NOT NULL,
  `users_password` char(32) NOT NULL,
  `users_nama` varchar(100) NOT NULL,
  `users_type` enum('admin','alternatif','penilai') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`users_id`, `users_name`, `users_password`, `users_nama`, `users_type`) VALUES
(1, 'admin', '5f4dcc3b5aa765d61d8327deb882cf99', 'admin', 'admin'),
(2, 'penilai1', '5f4dcc3b5aa765d61d8327deb882cf99', 'penilai1', 'penilai'),
(3, 'penilai2', '5f4dcc3b5aa765d61d8327deb882cf99', 'penilai2', 'penilai'),
(4, 'mapala', '5f4dcc3b5aa765d61d8327deb882cf99', 'UKM Mapala', 'alternatif'),
(5, 'kesenian', '5f4dcc3b5aa765d61d8327deb882cf99', 'UKM Kesenian', 'alternatif'),
(6, 'informatikakomputer', '5f4dcc3b5aa765d61d8327deb882cf99', 'UKM Informatika & Komputer', 'alternatif'),
(7, 'futsal', '5f4dcc3b5aa765d61d8327deb882cf99', 'UKM Futsal', 'alternatif'),
(8, 'basket', '5f4dcc3b5aa765d61d8327deb882cf99', 'UKM Basket', 'alternatif'),
(9, 'taekWondo', '5f4dcc3b5aa765d61d8327deb882cf99', 'UKM TaekWondo', 'alternatif');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `alternatif`
--
ALTER TABLE `alternatif`
  ADD PRIMARY KEY (`alternatif_id`),
  ADD KEY `alternatif_kriteria_id` (`alternatif_kriteria_id`),
  ADD KEY `alternatif_users_id` (`alternatif_users_id`);

--
-- Indexes for table `kriteria`
--
ALTER TABLE `kriteria`
  ADD PRIMARY KEY (`kriteria_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`users_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `alternatif`
--
ALTER TABLE `alternatif`
  MODIFY `alternatif_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `kriteria`
--
ALTER TABLE `kriteria`
  MODIFY `kriteria_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `users_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `alternatif`
--
ALTER TABLE `alternatif`
  ADD CONSTRAINT `alternatif_kriteria_id_fk` FOREIGN KEY (`alternatif_kriteria_id`) REFERENCES `kriteria` (`kriteria_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `alternatif_users_id_fk` FOREIGN KEY (`alternatif_users_id`) REFERENCES `users` (`users_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
